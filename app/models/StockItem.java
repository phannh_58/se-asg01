package models;

public class StockItem extends Model{
    @Id
    public Long id;

    public Warehouse warehouse;

    @ManyToOne
    public Product product;

    public Long quantity;

    public String toString(){
        return String.format("$d %s",quantity,product);
    }
}